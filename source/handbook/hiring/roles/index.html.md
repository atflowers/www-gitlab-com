---
layout: markdown_page
title: "Roles"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Role

For each role at GitLab, there should be one position description as it is the single source of truth. A role has the url /roles/title and contains the following paragraphs:

- Overview
- Levels (junior, intermediate, senior, staff, etc.) which describe the requirements for each level
- [Specializations](/roles/specialist) (CI/CS, distributed systems, Gitaly) relevant to that role
- Hiring process
- Apply
- About GitLab
- Compensation

The role does not contain:

- Locations (EMEA, Americas, APEC)
- [Expertises](/roles/expert) since these are free form.

The position description will be used _both_ for the [Vacancy Creation Process](/handbook/hiring/vacancies/#vacancy-creation-process), as well as serving as the requirements that team members and managers alike use in conversations around career development and performance management.

## New Role Creation

If a hiring manager is creating a new role within the organization, the hiring manager will need to create the role. If this is a role that already exists (for example, Gitaly Developer would use the Developer position description), update the current position description to stay DRY. If the compensation for the role is the same as one already in `/roles`, you should just update the specialty, do not create a new role.

1. Create the relevant page in `https://about.gitlab.com/roles/[name-of-role]`, being sure to use only lower case in naming your directory if it doesn't already exist.
1. Add each paragraph to the position description, for an example see the [developer role](/roles/engineering/developer)
1. Assign the Merge Request to your manager, executive leadership, and finally the CEO to merge. Also, cc `@gl-peopleops` for a compensation review.
1. Once the merge request has been merged, the People Ops Analyst will propose an appropriate compensation benchmark for the role.
1. The People Ops Analyst will send an email of the proposal to the Chief Culture Officer and Executive of the department for benchmark approval.
1. Once approved, the People Ops Analyst will add the benchmark to the `roles.yml` file which will automatically cause the [Compensation Calculator](/handbook/people-operations/global-compensation) to show at the bottom of the position description page.
