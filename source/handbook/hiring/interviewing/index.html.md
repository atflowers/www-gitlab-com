---
layout: markdown_page
title: "Interviewing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## How to Apply for a Position

The best way to apply for a position with GitLab is directly through our [jobs page](/jobs), where our open positions are advertised. If you do not see a job that aligns with your skillset, please keep your eye on the jobs page and check back in the future as we do add roles regularly. Please be advised that we do not retain unsolicited resumes on file, so you will need to apply directly to any position you are interested in now or in the future.

To apply for a current vacancy:

1. Go to our [jobs page](/jobs).
1. Click on the position title that interests you! Please also refer to the [country hiring guidelines](/jobs).
1. You will be redirected to a new page hosted by Lever, our Applicant Tracking System (ATS), where you can read the job description. If the position interests you, click "Apply for this Job".
1. You will be asked to fill out basic personal information, provide your resume and cover letter, and answer any application questions, as well as answer a voluntary Equal Employment Opportunity questionnaire.
1. Once you have finished, click "Submit Application" at the bottom.

## Typical Hiring Timeline

An applicant should expect to wait 4-5 business days between each step of the process, although longer timelines can occur. An applicant, at any time, is welcome to contact the People Ops team member they are working with for an update on their candidacy. These steps may vary role-to-role, so please review the hiring process per vacancy.

1. The employment team does a **first round of evaluations**. The employment team will also refer to the [country hiring guidelines](https://about.gitlab.com/jobs/#country-hiring-guidelines) before moving candidates forward. Disqualified candidates will be sent a note informing them of the [rejection](#rejecting-applicants). There are templates in Lever to assist, but messages can be tailored as appropriate. Place yourself on the receiving end of the message. If more information is required to make the determination, feel free to specifically ask for it (e.g. a cover letter). (If you don't see the templates, you probably haven't [linked your email account to Lever](https://help.lever.co/hc/en-us/articles/204485965-How-do-I-log-in-) yet.)
1. **Pre-screening Questionnaire**: Many applicants will be sent a pre-screening questionnaire by the employment team relating to the position to complete and return to the sender. The questionnaire and answers will be added to the candidate's Lever profile.
   1. Team members who review the pre-screening questionnaire answers should refer to the [GitLab private project](https://gitlab.com/gitlab-com/people-ops/applicant-questionnaires) that holds guides on how to review each of the questionnaires. Candidates who receive an assessment are moved to the "Assessment Sent" stage in Lever by a member of the Recruiting team.
   1. When a candidate returns their assessment, they are moved to the "Assessment in Review" stage in Lever by the Recruiting team, who will also either ping the appropriate team member to review the candidate's responses, or they will put a link to the candidate in the appropriate Slack channel dedicated to reviewing questionnaires. Reviewers in these Slack channels will add :eyes: emoji when they are reviewing and a :white_check_mark: emoji when they've reviewed a candidate. Reviewers should be sure to tag the appropriate Recruiting team member when leaving any notes in Lever.
   1. Candidates that have satisfactory assessment results may be invited to a screening call.
1. [**Screening call**](#screening-call):
   1. If the applicant qualifies, a member of the employment team will conduct a screening call using Lever. When a canidate clicks on the Easy Book link, it will show them the timezone of the Recruiter who sent them the link. They will need to change it to their local timezone to see the correct times. Candidates can do this by clicking on the timezone field at the top of the screen.
   1. A member of the employment team will move the applicant to the "screening call" stage in Lever.
   1. Our [recruiters](https://about.gitlab.com/roles/recruiter/) will do a screening call;
depending on the outcome of the screening call, the employment team or manager can either [reject an applicant](#rejecting-applicants) or move the applicant to the interview stage in Lever.
   1. The recruiter will wait 5 minutes for the candidate to show up to the appointed video call link, which is always shared with the candidate via email. If the candidate does not show up to the interview or reach out in advance to reschedule, the candidate will be classified as a "no show" and be disqualified.
   1. After the screening call, the Recruiter will verify the candidate is not on any known [Denied Party List](https://www.export.gov/csl-search). If the candidate is on a list, the application process will end.
1. **Behavioral interview**: Some roles include a behavioral interview with a team peer or leader.
1. **Technical interview (optional)**: Certain positions also require [technical interviews](/handbook/hiring/interviewing/technical).
1. **Further interviews**: Additional interviews would typically follow the reporting lines up to the CEO. For example, the technical interview may be conducted by an individual contributor, with subsequent interviews being conducted by the manager / team lead, executive team member, and then potentially the CEO. The candidate should be interviewed by at least one female GitLab team member. Interviewers will follow the same "no show" policy as the recruiters; if a candidate does not show up or reach out to the team, they will be disqualified. All interviewers will complete an interviewing training, which will be assigned to them from People Ops, and can be found in the [People Ops Employment issue tracker](https://gitlab.com/gitlab-com/people-ops/employment).
1. **References**: The hiring manager will conduct [references](#reference-check-process) for promising candidates. The references will be collected at the application stage, but they will not be contacted until later on in the process. This should happen before an offer is made. Three references will be asked for but at minimum two references should be completed - at least one should be a manager, the others to a colleague. The Recruiting team will move the candidate to the "Reference Call" stage in Lever.
1. **Hiring package**: After reference calls are completed successfully, the employment team submits the [hiring approval package](#hiring-package) to the CEO and Hiring Manager.
1. When the manager or CEO makes the **offer**, this can be done verbally during the call with the applicant, but is always followed quickly with the written offer by the Recruiting team as described in [the section on preparing offers and contracts](#prep-contracts). At this point the candidate is moved to the "Offer" stage by the Recruiting team.
1. People Ops will draft and stage a contract based upon the written offer that was extended.
1. People Ops will, if applicable, add language to the contract that states the employment or engagement is contingent on a valid work permit or visa. A start date should factor in that the approval of a new work permit may take several weeks.
1. Manager follows up to ensure that the offer is accepted, and that the contract is signed.
1. People Ops [starts the background check process](/handbook/people-operations/code-of-conduct/#background-checks) if applicable for the role.
1. People Ops [starts the onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).
1. Manager considers [closing the vacancy](https://about.gitlab.com/handbook/hiring/vacancies/#closing-a-vacancy).

## Screening Call

We conduct screening calls for all positions. This call will be completed by our [recruiters](https://about.gitlab.com/roles/recruiter/).

Questions include:

1. Why are they looking for a new position?
1. Why did they apply with GitLab?
1. What are they looking for in their next position?
1. Why did they join and leave their last three positions?
1. What is their experience with X? (for each of the skills asked in the position description)
1. STAR questions and simple technical or skills-related questions
1. What is their current location and do they have any plans to relocate? (relevant in context of comp, and in case an offer would be made)
1. Do they require a visa/residence permit to be able to work for GitLab?
1. What is the notice period they would need if they were hired?
1. What are their compensation expectations?
1. Confirm full legal name (relevant in case an offer would be made)

At the end of the screening call, the applicant should be told what the next steps would be and the timeline.
An example message would be "We are still reviewing applications, but our goal is to let you know in 5 business days from today whether you've been selected for the next round or not. Please feel free to ping us if you haven't heard anything from us by then."

## Moving Applicants Through The Process

**Remember to inform applicants about what stage they are in.** So, for example, if in the hiring process for the particular position / team you've agreed that there will be four stages, be sure to inform the applicant of where they are in the process during each call / stage: "You are in stage X and will be moving to stage Y next." Some brief feedback from the previous stage can also be included to help the candidate gauge their progress.

**The process can differ from team to team and from position to position.** If an applicant submits a resume to a particular open position and is being considered for another open position, send a short note to update and approve with the candidate, as well as inform them that their process may be slightly different or delayed. If the roles are on different teams, the applicant will ideally only move forward with one, depending on their interests and qualifications.

**Recruiters will schedule the next person in the process.** Someone on the recruiting team will move candidates forward to the next person in the hiring process if the candidate has received positive feedback.

**Compensation is discussed at start and end but not in between.** Compensation expectations are asked about during the [screening call](#screening-call). If the expectations seem unworkable to the manager or recruiter (based on what had been approved by the compensation committee at the [creation of the vacancy](https://about.gitlab.com/handbook/hiring/vacancies/#vacancy-creation-process), then the recruiter can send a note to the candidate explaining that salary expectations are too far apart, but they should also ask how flexible the candidate is and if they would consider adjusting their expectations. If expectations are aligned, then the topic of compensation should not re-surface until an [offer is discussed internally](#offer-authorization). Following this guideline avoids conflating technical and team interviews with contract discussions and keeps the process flowing smoothly.

If the manager has a question about compensation, please ping the People Ops Analyst for review. If the question needs to be escalated, the People Ops Analyst will add the Chief Culture Officer to the conversation.

**The CEO authorizes all offers.** <a name="offer-authorization"></a>The manager proposes a suggestion for an offer (including bonus structure if applicable, etc., using the [global compensation framework](/handbook/people-operations/global-compensation)) as an internal comment in Lever and informs the CEO on its details depending on what is applicable. The recruiting team will create a [hiring package](#hiring-package) to present to the CEO for approval.

Be aware that the visibility of internal comments in Lever are only available to [certain team members](https://help.lever.co/hc/en-us/articles/205644649-What-is-the-difference-between-access-roles-in-Lever-).

## Conducting a GitLab Interview

Interviewing is hard for both sides. In less than one hour you both need to get to know each other and both will have to make the decision if you want to work with this person.
This is an effort to provide a set of guidelines to make interviewing a bit less traumatizing.

Note: So you are about to interview folks for a job at GitLab? Please take a moment to carefully read
[this document on keeping it relevant and legal, including a self-test](https://docs.google.com/document/d/1JNrDqtVGq3Y652ooxrOTr9Nc9TnxLj5N-KozzK5CqXw) (please note this document is internal to GitLab while we edit it to make it fit for general audiences). For example, if there is a gap in employment history on a CV, you can ask the candidate what they did during that time to keep their skills current. You may not ask why they were absent from work as it may be related to a medical or family issue which is protected information.

When discussing the interview process with candidates, it is encouraged to set context for the interview (such as providing links to the handbook and if the interview will be a behavioral or technical interview, etc.), but GitLabbers should not prep candidates for specific interview questions.

New internal interviewers will partake in an [interviewing training](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/interviewing.md), which will be assigned by Recruiting. As part of the training, team members will shadow a recruiter and be shadowed by them, in order to make sure all GitLabbers are following our interviewing process and creating an excellent candidate experience. The recruiter who will work with the team member should be either aligned to their timezone or the role they'll be helping interview for. Feel free to ping `@gl-hiring` in your training issue if you are not sure which recruiter to contact, or send a message in the `#hiring` channel in Slack.

### Before The Interview

* Screening - writing a good resume is an art, and not many people master it. When you read a resume look for evolution rather than buzzwords, and if something sparks your curiosity, ask.
* If the process is taking too long, apologize and explain what is going on. It is really frustrating to not hear anything from the other side, and then continue conversations like nothing has happened. Show respect for the time of the candidate.
* If the process ends at this stage, be kind, and if the interviewee asks for feedback, give honest constructive feedback that explains why we have made our decision.

### During The Interview

1. There is an unbalanced power relationship during the interview. The interviewer is in a powerful position. They will decide if this candidate will get an offer or not. Be mindful of this. Be as friendly and approachable as you can. Be frank about what is going on, explain how the interview is going to be and set clear expectations: tell it like it is. This has the added value of getting people comfortable (over time) and allows you to get much better data.
1. Communication is really hard, don't expect perfect answers. Every person is different and they will express things differently, they are not listening to your train of thought so they will say things differently than what you expect, work on approaching to what they are trying to say rather than demanding them to approach to you. Once you have an answer validate your assumptions by explaining what you understood and allow the candidate to correct your story.
1. Don't go checking for perfect theoretical knowledge that the interviewee can google when needed, or give a problem that took you 2 months to dominate yet you expect your interviewee to master in a 30 minutes conversation. Be fair.
1. Aim to know, at the end of the interview, if you want to work with this person.
1. Interview for soft skills, really, do it. Pick some behavioral questions to get data on what has the candidate done before and how their behavior aligns to the company values. We are all going to be much happier if we all naturally agree on how things should be.
1. Consider having more people interviewing with you, different people see and value different things. More data helps making better decisions and ends up being a better use of interviewing time for both the candidate and the company.
1. Always let the interviewee ask questions at the end, and be frank in your answers.

### Technical Interview Considerations

1. Try to get a real sample of work (which we already do for developers by working on GitLab issues). Avoid puzzles or weird algorithm testing questions. Probing for data structures is fine as long as it is relevant to the job the person is going to do.
1. Be mindful of the background of the candidate, someone who knows 10 languages already (and some languages in particular, Perl for ex), may pick up Ruby in a second given the right chance. Don't assume that someone with a Java background will not be capable of moving to a different stack. Note that individual positions may have stricter requirements; the Backend Developer position [requires Ruby experience](/roles/engineering/developer/), for example.
1. Consider including non-engineering people to ask soft skills questions. Because technical people should be capable of talking to non-engineering people just fine, we should assess it.

### Candidate Performance Evaluation

The goal of behavioral (STAR) questions is to get the candidate to share data on past experiences. Previous behavior is considered the most effective indicator of how a person is going to act in the future.

The questions are usually in the form of: "Can you tell me about a time when...". The kind of answer that we are looking for is to get a story that is structured following the Situation, Task, Action, Result. Ask for an overview, an executive summary, of the case at hand. Avoid lengthy answers from the candidate at this stage.

Some things to pay attention to:

* What the candidate chose to highlight in the case as important
* Is it clearly explained? Is the story well told? If it is a technical story and the interviewer is a non-technical person, are things being explained in a way that is easy to understand?
* Is there a result or was the story left unfinished? Is it still going on?
* Was the result measured in any way? How does the candidate validate the result matched the expectation? Was there an expectation set to begin with?

There is no right answer, what matters here is to hear the candidate and gather data on how they are telling the story.

Once you have your notes, tell the candidate what you understood, repeat the story, and let them correct you as needed.


After having a high-level understanding of the case, we will want to dive deeper into the details. The objective of this step is to understand and detail the exact contributions a candidate has made to an effort which led to results. We will take a reverse approach to the STAR question structure presented earlier.

The key to analyzing each of the reverse-STAR steps is to ask _What, Why, How and Who_ at each step of the process. This will let the candidate paint a very clear picture of the situation, their ownership of idea/solution and the decision process in key pivotal moments. Reverse the order of the STAR structure and drill up from results to the situation as a whole. Find the answer to the following questions:
1. What was the goal to achieve or the problem to overcome? What was the expectation? Was the goal defined from the get-go?
1. How was the result measured? Why was it measured that way?
1. What steps or process was followed to achieve the result? List them together with the candidate
1. Who else was working with the candidate? Was the candidate working alone?
1. What role did the candidate have in the team, if not alone on the project? Was the candidate in charge of specific tasks? Who decided on task assignment? What was their impression of the tasks? How were the tasks decided on?
1. For the tasks discussed above, understand if there were resources who helped the candidate and at what capacity. How were those chosen and why?

These questions can be quite unbalancing and can increase the stress during the interview. Again, be kind and help the candidate understand what are you looking for, provide an example if one is needed when you notice the candidate is blocked.

It can also happen that the candidate does not have a story to share with you, that is OK. It's just another data point that should be added to the feedback (I failed to get data on ...), just move to the next question, just be sure to have a few questions as a backup.

These questions should be aligned with our company values. What we are looking for is understanding how this candidate behaves, and if this behavior matches the one we look for in our company values.

### Interview Feedback

Always leave feedback, this will help everyone to understand what happened and how you came to your decision. In Lever, when interviewing a candidate, please leave feedback in the [custom feedback forms](https://help.lever.co/hc/en-us/articles/206403285-How-do-I-create-interview-feedback-forms-) created in Lever. Currently, most roles do not have custom feedback forms, so we are using the "General feedback" form for most candidates. Part of the feedback form will ask for a score - please do leave a score for each candidate.

Scoring can be defined as follows:

  - 4 - Very likely to hire (meets most requirements, aligns with values)
  - 3 - Semi-Inclined to Hire (may meet some requirements, has some yellow flags)
  - 2 - Not likely to hire (meets few requirements, has many yellow flags, may not align with values well)
  - 1 - Would not hire (does not meet requirements, red flags, not aligned with values)

## Rejecting Applicants

1. At any time during the hiring process the applicant can be rejected.
1. The applicant should always be notified of this. The employment team is primarily
responsible for declining candidates.
1. If the applicant asks for further feedback only offer frank feedback. This
is hard, but it is part of our [company values](/handbook/values).
    * All feedback should be constructive and said in a positive manner. Keep it short and sweet.
    * Feedback should always be applicable to the skill set of the position the candidate applied and interviewed for.
    * Feedback and rejection should always be based on the job requirements.
    * If you feel uncomfortable providing feedback for whatever reason, reach out to People Ops for assistance.
    * Suggested feedback format: "The reason we don't think you're the best match for this position is __ . We are impressed with your skill in __ . That we decline you doesn't mean that you are not a good fit for this position. We receive over 1000 applications per month and have to decline almost all applicants. Both Facebook and Twitter rejected the founder of Whatsapp https://thehustle.co/whatsapp-founder-got-rejected-by-both-twitter-and-facebook-before-19-billion-buyout. We don't think we'll do any better and look forward to hearing from you after landing a better job or starting a successful company. Thanks for your interest in working at GitLab." This format can be used as a guideline to help candidates understand our decision, but can be personalized/customized to fit each situation. Personalization in communication with candidates is encouraged.
1. If people argue with the feedback that we provided:
    * Do not argue with or acknowledge the validity of the contents of the feedback.
    * Share their feedback with the people involved in the interviews and the decision.
    * Template text: "I've shared your feedback with the people involved in the interviews and the decision. We do not expect to revert the decision based on your feedback. In our hiring process we tend to error on being too cautious. We rather reject someone by mistake than hire someone by mistake, since a wrong hire is much more disruptive. Organizations can reject people with great potential http://thehustle.co/whatsapp-founder-got-rejected-by-both-twitter-and-facebook-before-19-billion-buyout so please don't be discouraged from seeking a great job."
1. The employment team may send out an inquiry to candidates to gather feedback after they have exited the hiring process.
   * People Ops will review all feedback and use it to improve the hiring process.

## Hiring Package

The employment team will create an employee approval package for CEO for each candidate in Lever after their interview with an executive and their reference checks are completed. The CEO will review the approval package and tag the candidate as Approve, Question, Interview, Decline.

   * Approve = Make the offer
   * Question = Discuss offer with hiring manager
   * Interview = Proceed with final CEO interview
   * Decline = Decline candidate

Leave a secret comment in Lever with below hiring package template filled out and cc the hiring manager and CEO. Ping the CEO in chat with "Hiring approval needed for [Position]" with a link to the Lever comment.

   Request approval for the following hire
   - Level and position:
   - Interviewers and their score:
   - Flags raised:
   - Location:
   - Candidate's salary expectation beginning of process:
   - Suggested Comp and stock options:
   - Experience factor:
   - Comp Calculator url:
   - References checked:
   - Hiring manager:
   - Link to vacancy:
   - Link to role page:
   - In case of interview is it OK to make the offer as CEO:

Please make sure that the level and position match the role page.

The CEO should make sure to mention the person making this comment both when giving a response and when posting any interview notes.

## Getting Offers and Contracts Ready, Reviewed, and Signed
{: #prep-contracts}

Offers made to new team members should be documented in Lever through the email thread between the person authorized to make the offer and the applicant.

1. Email example is in the "Offer letter" template in Lever. When using the template:
   1. make sure that you offer the correct [contract type and entity](/handbook/contracts/#how-to-use), ask People Ops if in doubt;
   1. include the People Ops alias in the cc (when you are ready for a written contract to be made), and
   1. change the subject line of the email. When making multiple hires for the same position, having the same email subject line can cause confusion in a Gmail inbox that collects conversation threads based on subject line. So for example make it "{first name of applicant} - offer for {position name} at GitLab", and be sure to send the email from Lever.
   1. Note: the number of proposed stock options must always be mentioned specifically, even when it is 0.
1. One person from People Operations will reply-to-all to everyone in the thread (including the applicant) to confirm that they will make the contract. Speed matters: if you are in People Operations and you can tackle this, then raise your hand and hit reply-all.
1. This person from People Operations
   1. checks all aspects of the offer:
      - was it approved by the CEO?
      - do the contract type and entity make sense?
      - is it clear how many (if any) stock options this person should receive?
      - is all necessary information (start date, salary, location, etc.) clearly available and agreed to?
      - does the candidate need a work permit or visa, or require an update to them before a start date can be agreed?
   1. makes the contract based on the details found in the Lever platform, using reply-all to gather any missing pieces of information,
   1. has the contract reviewed and approved by another member of People Ops as a quality check. The People Ops team member who did not create the contract will conduct the quality check. Backups for approval are the People Ops Generalist, then the People Ops Analyst, and lastly the Chief Culture Officer.
   1. stages the contract in HelloSign which emails the contract to the signing parties, cc People Ops and the hiring manager
   1. emails the candidate to let them know they've staged the contract via HelloSign and once the GitLab signatory has viewed and approved they will be prompted to do the same
1. When the contract is signed, the People Ops team member should move the candidate in Lever to the "Hired" bucket, which is only accessible to Lever Super Admins. Thanks to an integration between Lever and BambooHR, it will automatically add an entry for the new team member in BambooHR. However, in the automatic move, "self-service" is switched off in BambooHR by default, so this must be switched on explicitly within BambooHR.
1. This same person from People Operations files the signed contract in the appropriate place, and starts the [onboarding issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md).
1. Candidates will start the onboarding process no more than 30 days before her/his start date.

## CEO Interview Questions

The questions are available in a [Google form](https://docs.google.com/forms/d/1lBq_oXaqpQRs-SeEs3EvpxFGK55Enqn_nzkLq2l3Rwg/viewform) which can be used to save time during the actual interview.
All candidates are asked to fill out the form when they are scheduled for an interview with our CEO to discuss during their call with the CEO.

## Reference Check Process

**Why Should Hiring Managers Check References**
 - To ensure we are hiring the right candidate.
 - To understand under what circumstances this candidate thrives vs what circumstances frustrates them.
    - As the hiring manager, you will be the closest to the GitLabber and benefit most from learning about them.
-  To build your network.
    - As a hiring manager, you need to build a network for great talent.  Each reference you talk to can be a part of that network.  
- To use the opportunity to spread the GitLab story.  We could spark new customers and new GitLabbers.

All GitLab Hiring Managers should be making the best effort to complete and conduct the reference checks for their candidate. As part of our hiring process we will ask applicants to provide us with three references to contact (at least one should be a manager). At a minimum two references should be completed.  We will ask for these details at the beginning of the hiring process. When a candidate passes the initial screening and first rounds of interviews, before they advance to meet with senior leadership, the hiring manager should reach out and contact the references provided. If it is not possible to schedule a call via phone, a hangout or zoom meeting can also be arranged if it's convenient. If that is not possible an email can be sent with the following questions:

- Can you briefly describe your working relationship with them?
- What are they like to work with?
- What can they improve upon?
- What advice would you give their next hiring manager?


You can also elaborate further and ask follow-up questions if the opportunity arises. The hiring team will be adding engineering questions into Lever so that all engineering hiring managers have access to the same questions. Additionally, the hiring team will work with each function to identify any other specific questions hiring managers would like to add to Lever for their team.  

You should not ask any questions about the person's race, gender, sexual preference, disabilities or health, political affiliations, religion, family (children). Example questions not to ask:
- Who watches their children while they are at work?
- What types of groups does the candidate belong to that are not work related?

All reference check feedback should be entered into Lever using the Reference Checks form. To add this form, go to the candidate's profile, click the three dots "..." next to the notes section, click "Add Form", and choose "Reference Checks".

It is the hiring manager's responsibility to do the reference checks but the hiring team can also provide assistance and guidance. You can also refer to [these guidelines](http://www.bothsidesofthetable.com/2014/04/06/how-to-make-better-reference-calls/).
Increasingly, organizations have a company policy that prevents their employees from providing references; instead, they are only able to verify employment, including dates of employment and title.  Do not judge a candidate because his or her former employer has this policy; it does not mean the candidate was not successful.  Instead, go back to the candidate to get the name and contact informatoin for an alternative reference.  

## Background Checks

Team members in the certain positions must partake in a [background check](/handbook/people-operations/code-of-conduct/#background-checks), which covers criminal and employment history.

Candidates who are in Support Engineering, Customer Success, People Ops, Finance, Sales (client-dependent), and the Executive team will be sent the link to the background check after the contract is signed. The new team member may start with GitLab prior to the returned check, but their continued employment will be contingent on a clear returned check.
