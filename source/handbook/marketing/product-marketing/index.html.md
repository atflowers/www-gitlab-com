---
layout: markdown_page
title: "Product Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Roles

- **PM** - Product Management or Product Manger. The [product team](https://about.gitlab.com/handbook/product/) as a whole, or the specific person responsible for a product area.
- **PMM** - Product Marketing Management or Product Marketing Manager. The product marketing team as a whole, or the specific person responsible for a product marketing area.

## What is PMM working on?
- View the [Product Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/boards/397296?=&label_name[]=Product%20Marketing) to see what's currently in progress.
- To ask for PMM resources log an issue in the marketing project and label it with `Product Marketing`.
- The PMM team does sprint planning on a biweekly basis where the backlog is reviewed and issues are prioritized.
- If you need more immediate attention please send a message in the `#product-marketing` slack channel.

## Release vs Launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then releases it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |

## Tiers

### Overview

| Tier      | Delivery                  | License                   | Fee            | Time      |
| --------- | ------------------------- | ------------------------- | -------------- | --------- |
| Libre     | Self-hosted               | Open Source               | Gratis         | Unlimited |
| Trial     | Self-hosted               | Proprietary               | Gratis         | Limited   |
| Starter   | Self-hosted               | Proprietary               | Paid           | Unlimited |
| Premium   | Self-hosted               | Proprietary               | Paid           | Unlimited |
| Ultimate  | Self-hosted               | Proprietary               | Paid           | Unlimited |
| Free      | GitLab.com                | Open Source               | Gratis         | Unlimited |
| Pilot     | GitLab.com                | Proprietary               | Gratis         | Limited   |
| Bronze    | GitLab.com                | Proprietary               | Paid           | Unlimited |
| Silver    | GitLab.com                | Proprietary               | Paid           | Unlimited |
| Gold      | GitLab.com                | Proprietary               | Paid           | Unlimited |

### Definitions

1. Users: anyone who uses GitLab regarless of tier.
1. Customers: users on a paid tier.
1. Plans: the paid tiers only.
1. Time-limited tiers: pilots (GitLab.com) and trials (self-hosted).
1. Subscription: a combination of the paid tiers and the time-limited tiers, for example the trial tier is a subscription but not a plan.
1. License: open source vs. proprietary, for example moving a feature from a proprietary tier to the open-source tier.
1. Tier: a combination of the subscription tiers and the open source licensed tiers.
1. Distribution: self-hosted CE vs. EE, for example you can have a EE distribution but in the Libre tier.
1. Version: the [release of GitLab](https://about.gitlab.com/releases/), for example asking what version a user is on.

### Delivery

In general each of the five self-hosted tiers match the features in the GitLab.com tiers. They have different names for two reasons:

1. There is not complete feature parity between self-hosted and GitLab.com plans. For example, Starter, Premium, and Ultimate include [LDAP Group Sync](https://docs.gitlab.com/ee/administration/auth/ldap-ee.html#group-sync) but Bronze, Silver and Gold do not.
1. We want to know if a user is using self-hosted or GitLab.com based on a just the tier name to prevent internal and external confusion.

When we need to say what  tier a feature is in (for example in our documentation and release post) we use the self-hosted tiers because they tend to contain a superset of the GitLab.com tier features.

### Libre, Gratis, and Free
Libre, Gratis, and Free are terms used in the open source community. "Free" is an ambiguous term that can means either free as in "no cost" (e.g. $0 "free as in beer"), free as in "with few or no restrictions" (e.g. "free as in free speech"), or both. "Gratis" is an unambiguous term to mean "no cost" while "Libre" is an unambiguous term to mean "with few no restrictions." Open source software is "libre" in that it is free to inspect, modify, and redistribute. Open source software may or may not be "gratis." Our time-limited tiers are gratis but not open source. Our Free and Libre tiers refer to our open source software that is both [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). For more info see the [wikipedia article](https://en.wikipedia.org/wiki/Gratis_versus_libre).

### Personal vs Group subscriptions
GitLab.com subscriptions are added to either a personal namespace or a group namespace. Personal subscriptions apply to a single user while Group subscriptions apply to all users in the Group.

### Distributions
Libre users can use either one of two distributions: Community Edition (CE) and Enterprise Edition (EE). Enterprise Edition can be downloaded, installed, and run without a commercial subscription. In this case it runs using the open source license and only has access to the open source features. In effect, EE without a subscription, and CE have the exact same functionality. The advantage of using EE is that it is much easier to upgrade to a commercial subscription later on. All that's needed is to install a license key to access more features vs needing to re-install a different distribution.

**Note**: The terms CE & EE refer solely to the software distribution, not to the subscription plan. **Never use CE vs. EE as a substitute for versions.** Instead, talk about open source users vs. proprietary.

### GitLab Trial
Today, we only offer a [free trial for self-hosted GitLab](https://about.gitlab.com/free-trial/). The trial offers all of the features of Gitlab Premium. In the furutre the trial will [offer all the features of Ultimate](https://gitlab.com/gitlab-com/marketing/issues/2099). Users on the Libre (self-hosted) and Free (GitLab.com) plans get access for an unlimited amount of time to a limited set of features. Trial users get access to a full set of features for a limited amount of time (30-days).

| License type | Features | Time Period |
| ------------ | -------- | ----------- |
| Libre & Free | Limited (Open source features only) | unlimited |
| Trial        | Unlimited (access to al Premium features) | limited (30 days) |

In the future we might introduce a similar concept for GitLab.com. We'll call this a pilot to make sure you can tell the delivery mechanism from the tier.

### Open source projects on GitLab.com get all Gold features.
The GitLab.com Free plan offers unlimited public and private repos and unlimited contrbutores but has limted features for private repos. Private repos only get access to the open source features. Public projects get access to all the features of Gold free of charge. This is show our appreciation for Open Source projects hosted on GitLab.com.

### Messaging dos and don'ts

- Don't use the terms `Enterprise Edition Starter`, `Enterprise Edition Premium`, `Enterprise Edition Ultimate`, `EES`, `EEP`, or `EEU`. These have all been deprecated.
- Don't use `Community Edition`, `CE`, `Enterprise Edition`, or `EE` to refer to tiers.
- Don't use "edition" to refer to plans like `Starter Edition` or `Premium Edition` - Starter, Premium, and Ultimate are tiers, not "editions" of the software.
- Don't use "enterprise" as a modifier for tiers such as `Enterprise Starter`, `Enterprise Premium`, or `Enterprise Ultimate`.

- Do refer to plans by their stand-alone name: Libre, Starter, Premium, Ultimate, Free, Sliver, Bronze, and Gold.
- Do optionally use "GitLab" as a modifier for plan names: GitLab Libre, GitLab Starter, GitLab Premium, GitLab Ultimate, GitLab Free, GitLab Sliver, GitLab Bronze, and GitLab Gold.
- Do use "enterprise" to a market segment. e.g. good phrases to use are: "GitLab provides DevOps for the enterprise", "GitLab is enterprise-ready", "GitLab has many enterprise customers", and "GitLab provides enterprise software for the complete DevOps lifecycle."
- Do us `Community Edition`, `CE`, `Enterprise Edition` and `EE` to refer to our software distributions. Encourage customers to use the EE distribution since it provides the least painful upgrade path if/when users discover they need commercial features.
- Do use the terms "Open Source" and "Proprietary". Unlike other terms that are GitLab-insider language (e.g. "Free" vs "Bronze") almost everyone will immediately understand the difference. Calling our proprietary tiers closed source doesn't make sense because even our proprietary source code is publicly viewable.

## One deck to rule them all

### Prod
There is one canonical slide deck, known as "The Production Deck" that contains the GitLab narrative and pitch. The prod deck should always be in a state that is ready to present. This is a deck everyone should present from and/or fork from when there is a need to create new content tailored to a specific audience (analysts, events, specific customers, etc.). As such there should never be comments or WIP slides in the Prod deck. Only the pitch deck production engineer (William Chia) has write access to the production environment. Changes, comments, and additions should be made in the staging deck.

### Staging
To update the Staging deck, create a copy of the slide you want to modify, or add the new slide you want to suggest. Create an issue in the Marketing Issue tracker and label with 'Product Marketing' including a link to your slide and the title. The changes can the be discussed and revised until the slide is ready to be deployed to production.

### QA
Slides should go through QA first. This includes
- Spell and grammar check
- Technical accuracy check
- Links check (do links go to the right place)
- Brand / design review
- Slide notes are complete

## How to deploy a staging slide to production
When a slide has passed QA the slide deck production engineer should copy and paste it into the production deck. The change should be communicated in the #sales slack channel. Additionll enablement should be added to the weekly sales call if necessary.


## Competitive Intelligence

- Post any competitive info in the `#competition` slack channel.
- Link to the blog post, announcement, or feature page your are referencing.
- If you received the intel from a customer conversation, link to the Salesforce record and the full set of notes from the conversation.

## Customer Stories
A formal case study produces a polished, finished document. A "customer story" is a shorter, informal story about how a customer solved a particular problem or received a specific benefit. Case studies are usually made up of multiple customer stories for a single customer. Customer stories can also be anonymous. e.g. "A large international financial services company used Gitlab CI/CD to reduce build times from 1 hour to 10 mins."

Today, we don't have a central repository for customer service, but this is a Q1 goal to start building one. For now we are capturing customer stories in an issue. If you have a customer story or anecdote [leave a comment on issue 1834](https://gitlab.com/gitlab-com/marketing/issues/1834).

## Customer Case Study Creation

### Objectives:
- Illustrate successful and measurable improvements in the customer’s business
- Show other users how customers are using GitLab
- Align content so that GitLab solutions reflect the challenges our customers, prospects, and the market requirements
- Develop and grow organizational relationships: speak at events on our behalf, promote their business
- Support our value proposition - Faster from planning to monitoring

### Creating the Customer Case Study:

#### Kicking off a new case study
To start a new case study process, [create a new issue in the Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/issues/new?issue) and use the "Customer_Story_Kickoff" template.

**Explaining the Case Study process to a Customer & Creating the Case Study**:

Below is an example email that PMM may send customers to after the AE has introduced them to the customer. The email below will help the customer to get a better sense of what we are asking of them.

>Hi X,

>It's nice to e-meet you, we'd love to hear more about your journey to GitLab and potentially write a customer story on COMPANY NAME.

>Here are the steps that we'd work with you on.
>- 20-30 minute phone call to hear more about your industry, business, and team. (In the call, we would also like to hear more about your decision making process to first choose GitLab and then eventually purchase EE.)
>- Review of the draft customer story
>- Final review of the customer story Then once the customer story is agreed upon by you or someone on your team we will publish it on GitLab's channels.

>Please let me know if you're open to kicking off the customer story process on X date

### Collecting Metrics:
Possible quantitative metrics that the customer can be asked to share with GitLab include:
- Reduced cycle time
- Number of deploys in a given time frame
- Reduced number of bugs or Reverts
- Reduced number of admin hours
- Cost savings % through purchasing GitLab: Reduce the cost of managing a number of different people, projects, and platforms.
- Reduction in internal support tickets requests: Reduction in the number of support tickets team submitting to fix challenges, compared to initial SCM tool

The customer case study should then be written by Product Marketing team, and the draft sent to the customer for their input, and approval.

Other sections in the case study can be found on the customer's website or by searching the web - Sidebar summary, The Customer.

Following approval from the customer, the Design team should be sent a doc of the case study to include in the case study design template. The case study can then be published on our website.

### Case Study Format:

*Headline:* The name of the customer and the benefit they gained from implementing our solution

*Sidebar Summary:* Summarize key points
- Customer Name and Logo
- Customer Details: Country, Website
- Organization Type - Public/Private & Industry
- Annual Revenue - If publicly available
- Employees
- Summary of Key Benefits

*The Customer:* A brief introduction of the customer, not the solution.

*The Challenge:* What was the customer trying to change or improve. What regulations or market conditions drove the customer to search for a new solution. Share the steps the customer took to solve the problem, including other products and services they investigated.

*The Solution:* Detail the solution and how it aligns to the customers requirements. Also detail other solutions that GitLab interfaces with. Also include customer quote.

*The Results:* Detail how GitLab supported the customer to solve their problems. This is where the use of benchmarking metrics such as such as time saved, reduced costs, increased performance etc. are required.

*About GitLab:* Short paragraph on GitLab - about, solutions etc. Call to action of solutions offered.

*Possible Additional Supporting Documents:*
- Customer Deal Summary – High Level PowerPoint summary after deal first signed.
- Customer Success Overview
- Infographic – Single page A4 summary with diagrams and measurable benchmarks
- Benchmark Metrics
- Publish on website
- Video - Short video summary if customer is willing to participate - Perforce example
- Blog Post - Blog post to launch customer case study
- Keywords for SEO etc.

### Creating Case Study Blog Post:
Refer to the [guide available here](/handbook/marketing/marketing-sales-development/content/#casestudyblogposts).

## Analyst product categorizations

GitLab is a single application that spans many product categories.
Forrester and Gartner define several of these categories and their definition
of a space can be useful to determine what important competing products are.
Below some relevant categories that GitLab is or will be competing in.

### Forrester: Continuous Integration Tools

- Leaders: GitLab CI, CloudBees Jenkins, CircleCI, Microsoft
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Integration+Tools+Q3+2017/-/E-RES137261)

### Forrester: Configuration Management Software For Infrastructure Automation

- Leaders: Puppet Enterprise, Chef Automate
- [link to report](https://reprints.forrester.com/#/assets/2/675/'RES137964'/reports)

### Gartner: Software Change and Configuration Management Software

- Market Guide: Amazon Web Services, Atlassian, BitKeeper, CA Technologies, Codice Software, Collabnet, GitHub, GitLab, IBM, Micro Focus/Borland, Microsoft, Perforce, PTC, SeaPine Software, Serena, SourceGear, Visible Systems, WANdisco, Wildbit
- [link to report](https://www.gartner.com/document/3118917)

### Gartner: Software Test Automation

- Leaders: MicroFocus, Tricentis
- [link to 15 November 2016 report](https://www.gartner.com/document/3512920)
- [link to 20 November 2017 report](https://www.gartner.com/document/3830082)

### Forrester: Modern Application Functional Test Automation Tools

- Leaders: Parasoft, IBM, Tricentis, HPE
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Modern+Application+Functional+Test+Automation+Tools+Q4+2016/-/E-RES123866)

### Gartner: Performance Testing

- Market Guide: Automation Anywhere, BlazeMeter, Borland, CA Technologies, HPE, IBM, Neotys, Oracle, Parasoft, RadView, SmartBear, Soasta, Telerik, TestPlant
- [link to report](https://www.gartner.com/document/3133717)

### Gartner: Application Release Automation

- Leaders: Electric Cloud, CA (Automic), XebiaLabs, IBM
- [Magic qaudrant](http://electric-cloud.com/resources/whitepapers/gartner-magic-quadrant-application-release-automation/)

### Forrester: Continuous Delivery and Release Automation

- Leaders: Chef, CA, Microsoft
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Continuous+Delivery+And+Release+Automation+Q3+2017/-/E-RES137969)

### Gartner: Application Performance Monitoring Suites

- Leaders: New Relic
- [link to report](https://www.gartner.com/doc/3551918)

### Gartner: Application Security Testing

- Leaders: HPE, Veracode, IBM, Synopsys, WhiteHat Security
- [link to report](https://www.gartner.com/doc/3623017)

### Forrester: Application Security Testing

- Leaders: Synopsys, CA Veracode
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Static+Application+Security+Testing+Q4+2017/-/E-RES139431)

### Gartner: Project Portfolio Management

- Leaders: Planview, CA Technologies, Changepoint
- [link to report](https://www.gartner.com/document/3728917)

### Forrester: Strategic Portfolio Management Tools

- Leaders: AgileCraft, CA, ServiceNow
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Strategic+Portfolio+Management+Tools+Q3+2017/-/E-RES136707)

### Forrester: Portfolio Management For The Tech Management Agenda

- Leaders: CA, Planview
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Portfolio+Management+For+The+Tech+Management+Agenda+Q1+2015/-/E-RES114742)

### Gartner: Enterprise Agile Planning Tools

- Leaders: CA, Atlassian, VersionOne
- [link to report](https://www.gartner.com/doc/3695417)
- Research for next MQ commences Summer 2018

### Forrester: Enterprise Collaborative Work Management

- Leaders: Clarizen, Redbooth, Wrike, Planview, Asana, and Smartsheet
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Enterprise+Collaborative+Work+Management+Q4+2016/-/E-RES121721)

### Forrester: Application Life-Cycle Management, Q4 2012

- Covered in report: Atlassian, CollabNet, HP, IBM, Microsoft, PTC, Rally Software, Rocket Aldon, and Serena Software
- [link to report](https://www.forrester.com/report/The+Forrester+Wave+Application+LifeCycle+Management+Q4+2012/-/E-RES60080)

### Gartner: Container Management Software

- Market Guide: Apcera, Apprenda, CoreOS, Docker, Joyent, Mesosphere, Pivotal, Rancher Labs, Red Hat
- [link to report](https://www.gartner.com/document/3782167)

### Gartner: Enterprise Application Platform as a Service

- Leaders: Salesforce, Microsoft
- [link to report](https://www.gartner.com/document/3263917)

### Gartner: Data Science Platforms

- Leaders: IBM, SAS, RapidMiner, KNIME
- [link to report](https://www.gartner.com/doc/3606026)

## Partner Marketing

### Partner Marketing Objectives

- Promote existing partnerships to be at top-of-mind for developers.
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

For a list of Strategic Partners, search for "Partnerships" on the Google Drive.

### Partner Marketing Activation

Our partner activation framework consists of a series of action items within a high-level issue. When a strategic partnership is signed, Product Marketing will choose the issue template called [partner-activation](https://gitlab.com/gitlab-com/marketing/issues/new) which will trigger notification for all involved in partner activation activities.

For each action item within this issue, a separate issue should be created by the assignee who is responsible for that action item within the allocated timeframe.

The partner should be included on the high level issue so they can see the planned activities and can contribute.

### Partner Newsletter Feature

In line with the objective of: Promote existing partnerships to be at top-of-mind for developers, a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience. This feature should be co-authored by the partner.

Possible content:

- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested Format:

- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website

Creation:

Email potential partner with case for creating content/blog post which will feature in our newsletter. Also request that they include the content in their own newsletter.

Create separate issue for blog post in www-gitlab-com project with blog post label and assign to Content Marketing with the following information:
- A summary describing the partnership
- Any specific goals for the blog post (e.g. must link to this, mention this, do not do this...)
- Contacts from both sides for people involved in the partnership
- Any marketing pages or material created from both sides which we can use in the post
- Deadlines for the post (internal draft, partner draft review and publication)

After Publication: Send to partner a summary of the click-throughs to their website, registration for webinar etc.

## Channel Marketing

### Channel Marketing Objectives

- Support resellers to be successful and grow their business
- Motivate resellers to reach first for GitLab
- Promote [Reseller program](https://about.gitlab.com/resellers/program/) to recruit new resellers

### Reseller Funds Allocation Determination

- Resellers will request event, SWAG or campaign support by creating an issue using the [reseller issue template](https://gitlab.com/gitlab-com/resellers/issues/new). When the reseller has completed the issue template detailing their needs, Product Marketing and the Channel Reseller Director will be notified.
- When a reseller requests funds for online marketing campaigns, let them know that we can run the campaign in-house working with the Demand Generation team, and even provide artwork if required. All we need is the redirect links to the: /gitlab.com on their website.
- Post-event or campaign, set up a catch-up call with the reseller to determine ROI of GitLab investment.
- At the post event catch-up with the reseller, there are two tabs in the "GitLab events sponsorship request form (Responses)" sheet found on the google drive to be updated - one for events and the other online marketing campaigns.
- Request that the reseller send photos of the event, write a couple of short paragraphs on their experience at the event, any highlights, and impact on their business and GitLabs, and send to you. Photos and a short blog post could feature in the Reseller newsletter, the company-wide newsletter.
- After the Online Marketing campaign, we will send the reseller a summary from Google Adwords or equivalent with the metric measurements detailing the success of the campaign. Store campaign summaries in the "Online Marketing Campaign Summaries" folder on the Google Drive.
- As we gather more feedback, we will be able to assign a ROI to our marketing investments, and drive more SQLs.
