---
layout: job_page
title: "Engineering Management"
---

## Engineering Management Roles at GitLab

Managers in the engineering department at GitLab see the team as their product. While they are technically credible and know the details of what developers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of product commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Engineering Manager

* Hire a world class team of developers to work on their team
* Help their developers grow their skills and experience
* Code reviews, architecture, bugs, and small features
* Hold regular 1:1's with all members their team
* Create a sense of psychological safety on your team
* Recommend technical and process improvements
* Foster technical decision making on the team, but make final decisions when necessary
* Exquisite communication: Regularly achieve consensus amongst departments
* Author project plans for epics
* Draft quarterly OKRs
* Run agile project management process
* Train engineers to screen applicants and conduct managerial interviews
* Improve product quality, security, and performance

### Director of Engineering

The Director of Engineering role extends the [Engineering Manager](#engineering-manager) role.

* Hire a world class team of managers and developers to work on their teams
* Help their managers and developers grow their skills and experience
* Manage multiple teams and projects
* Hold regular skip-level 1:1's with all members of their team
* Create a sense of psychological safety on your _teams_
* _Drive_ technical and process improvements
* _Drive_ quarterly OKRs
* _Drive_ agile project management process
* _Own_ product quality, security, and performance
* Represent the company publicly at conferences

The Director of Engineering, Backend at GitLab manages multiple backend teams in the product development organization. They see their teams as their product. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both developers and managers alike. But they can also grow the existing talent on their teams. Directors are leaders that model the behaviors we want to see in our teams and hold others accountable when necessary. And they create the collaborative and productive environment in which developers and engineering managers do their work.

#### Responsibilities

- Hire and manage multiple backend teams that lives our [values](https://about.gitlab.com/handbook/values/)
- Measure and improve the happiness and productivity of the team
- Manage the agile development process
- Drive quarterly OKRs
- Work across sub-departments within engineering
- Own the quality, security and performance of the product
- Write public blog posts and speak at conferences

#### Requirements

- 10 years managing multiple software engineering teams
- Experience in a peak performance organization
- Deep Ruby on Rails experience
- Product company experience
- Startup experience
- Enterprise software company experience
- Computer science education or equivalent experience
- Passionate about open source and developer tools
- Exquisite communication skills

#### Nice-to-have's

- Kubernetes, Docker, Go, Helm, or Linux administration
- Online community participation
- Remote work experience
- Significant open source contributions

#### Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 45 minute first interview with our VP of Engineering
* Next, candidates will be invited to schedule a 45 minute second peer interview with our Director of Backend
* Next, candidates will be invited to schedule a 45 minute third interview with an Engineering team member
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

### VP of Engineering

The VP of Engineering role extends the [Director of Engineering](#director-of-engineering) role.

* Drive recruiting of a world class team
* Help their directors, managers, and developers grow their skills and experience
* Measure and improve the happiness of engineering
* Make sure the handbook is used and maintained in a transparent way
* _Sponsor_ technical and process improvements
* _Own_ the sense of psychological safety of the department
* _Set_ quarterly OKRs around company goals
* _Define_ the agile project management process
* Spend time with customers to understand their needs and issues
* _Be accountable for_ product quality, security, and performance

## Specialties

The Engineering Manager, CI/CD, directly manages the developers that develop
CI/CD features for GitLab.

See [Engineering Manager](/roles/engineering-manager/) for more details.

### CI/CD

#### Hiring Process

Applicants for this position can expect the hiring process to follow the order
below. Please keep in mind that applicants can be declined from the position
at any stage of the process. To learn more about someone who may be conducting
the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Recruiting team
* Next, candidates will be invited to schedule a 45 minute first interview with a member of the CI/CD team
* Next, candidates will be invited to schedule a 45 minute second interview with the Director of Backend
* Next, candidates will be invited to schedule a 45 minute third interview with our VP of Engineering
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html) is an
enterprise product feature set that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well. As the Engineering Manager for the Geo team, you will take overall responsibility for
hiring, team management, agile project management, and the quality of the
feature set. This position will report to the Director of Backend.

See [Engineering Manager](/roles/engineering-manager/) for more details.

### Geo

#### Responsibilities

As an Engineering Manager you are expected to
- Be a steward of product quality
   - Author project plans for epics
   - Run agile project management processes
   - Conduct code reviews, and make technical contributions to product
   architecture as well as getting involved in solving bugs and delivering small
   features
- Be a leader for the team
   - Draft quarterly OKRs and have regular 1:1’s with team
   - Actively seek and hire globally-distributed talent
   - Conduct managerial interviews for applicants, and train the team to screen applicants
   - Contribute to the sense of psychological safety on your team
   - Generate and implement process improvements

For the Geo team specifically, you will be involved in - and responsible for -
- Architecting Geo and Disaster Recovery products for GitLab
- Identifying ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
- Instrument and monitor the health of distributed GitLab instances
- Educate all team members on best practices relating to high availability

#### Requirements

- 5 years or more experience in a leadership role with current technical experience
- Experience architecting and implementing fault-tolerant, distributed systems
- In-depth experience with Ruby on Rails, Go, and/or Git
- Excellent written and verbal communication skills
- You share our [values](/handbook/values), and work in accordance with those values
- [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.

#### Hiring process

The hiring process for this position includes

- A screening call with one of our Recruiters
- An interview with the interim Engineering Manager - Geo
- An interview with the Director of Backend
- An interview with a Senior Developer with deep knowledge of the Geo feature set
- An interview with the VP of Engineering



<%= partial "includes/apply_and_about" %>
