---
layout: job_page
title: "Developer"
---

## Developers Roles at GitLab

Developers at GitLab work on our product. This includes both the open source version of GitLab, the enterprise editions, and the GitLab.com service as well. They work with peers on teams dedicated to areas of the product. They work together with product managers, designers, and backend or frontend developers to solve common goals.

### Junior Developer

Junior Developers share the same requirements outlined below, but typically join with less or alternate experience in one of the key areas of Developer expertise (Ruby on Rails, Go, Git, reviewing code).

### Intermediate Developer

* Write good code
* Catch bugs and style issues in code reviews
* Ship small features independently
* Be positive and solution oriented
* Good communication: Regularly achieve consensus with peers, and clear status updates
* Constantly improve product quality, security, and performance

### Senior Developer

The Senior Developer role extends the [Developer](#developer) role.

* Write _great_ code
* Teach and enforce architectural patterns in code reviews
* Ship _medium_ features independently
* Generate architecture recommendations
* _Great_ communication: Regularly achieve consensus amongst _teams_
* Perform technical interviews

***

A Senior Developer may want to pursue the [engineering management track](/roles/engineering/engineering-management) at this point. See [Engineering Career Development](/handbook/engineering/career-development) for more detail.

***

### Staff Developer

The Staff Developer role extends the [Senior Developer](#senior-developer) role.

* Write _exquisite_ code
* Ship _large_ features independently
* Make architecture decisions
* Implement technical and process improvements
* _Exquisite_ communication: Regularly achieve consensus amongst _departments_
* Author technical architecture documents for epics
* Train others to perform technical interviews and author code tests (where applicable)
* Write public blog posts

### Distinguished Developer

The Distinguished Developer role extends the [Staff Developer](#staff-developer) role.

* Ship _large_ feature sets with team
* _Generate_ technical and process improvements
* Contribute to the sense of psychological safety on your team
* Work cross-departmentally
* Be a technical mentor for developers
* Author architecture documents for epics
* Hold team members accountable within their roles

### Engineering Fellow

The Engineering Fellow role extends the [Distinguished Developer](#distinguished-developer) role.

* Work on our hardest technical problems
* Ship _extra-large_ feature sets with team
* Help create the sense of psychological safety in the department
* Represent the company publicly at conferences
* Work directly with customers

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Platform/Discussion

The [Platform](https://about.gitlab.com/handbook/product/#platform) and
[Discussion](https://about.gitlab.com/handbook/product/#discussion) teams are
responsible for working on the main Rails application in GitLab, with different
focuses. Both teams do very similar work, and share a hiring pool.

#### Ruby experience

For this position, a significant amount of experience with Ruby is a **strict**
requirement.

We would love to hire all great backend developers, regardless of the language
they have most experience with, but at this point we are looking for developers
who can get up and running within the GitLab code base very quickly and without
requiring much training, which limits us to developers with a large amount of
existing experience with Ruby, and preferably Rails too.

For a time, we also considered applicants with little or no Ruby and Rails
experience for this position, because we realize that programming skills are to
a large extent transferable between programming languages, but we are not
currently doing that anymore for the reasons described in the [merge
request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2695) that
removed the section from this listing that described that policy.

### Build

Build team closely partners with the rest of the engineering organization to build, configure, and automate GitLab installation.
GitLab's build team is tasked with creating a seamless installation experience for customers and community users across multitude of platforms.

Build engineering is interlaced with the broader development team in supporting newly created features.
Notably, the infrastructure team is the build team's biggest internal customer, so there is significant team interdependency.
Build team also provides significant variety in tasks and access to a diversity of projects, including helping out on various community packaging projects.
This is reflected in the job requirements: we are tasked with creating and maintaining a Cloud Native GitLab deployment and upgrade methods, Omnibus GitLab package installation across multiple Linux based Operating Systems, and various Cloud providers deployment methods (such as AWS Cloudformation, GC Deployment Manager and so on).

* Requirements
  * Experience with Docker and Kubernetes in production use cases
  * Chef experience (writing complex cookbooks from scratch, custom providers, custom resources, etc.)
  * Ruby/RoR experience
  * Extensive Linux experience, comfortable between Debian and RHEL based systems
  * Basic knowledge of packaging archives such as .deb and .rpm package archives
  * An inclination towards communication, inclusion, and visibility
  * Experience owning a project from concept to production, including proposal, discussion, and execution.
  * English written and verbal communication skills
  * Demonstrated ability to work closely with other parts of the organization
  * Share our values, and work in accordance with those values

### Security

Focus on security features and security products for GitLab. This role will report to and collaborate directly with our CTO.

1. Responsibilities
  - Develop security products from proposal to polished end result.
  - Integrating 3rd party security tools into GitLab.
  - Key aspects of this role are focused on security products and features.
  - The complexity of this role will increase over time.
  - If you are willing to stick to working on these features for at least a year, then this role is for you.
1. Requirements
  - Strong Ruby developer with security expertise or proven security interest.
  - Passion and interest toward security (scanning, dependencies, etc.).
  - Experience in using GitLab and GitLab CI.
  - Ability to work in European timezone.
1. Additional Hiring Process
  - Candidates will be invited to schedule a 60 minute interview with our CTO as the the technical interview.

### CI/CD

CI/CD Backend Developers are primarily tasked with improving the Continuous Integration (CI)
and Continuous Deployment (CD) functionality in GitLab. Engineers should be willing to learn Kubernetes and Container Technology. CI/CD Engineers should always have three goals in mind:
1. Provide value to the user and communicate such with product managers,
2. Introduce features that work at scale and in untrusting environments,
3. Always focus on defining and shipping [the Minimal Viable Change](https://about.gitlab.com/handbook/product/#the-minimally-viable-change).

We, as a team, cover end-to-end integration of CI/CD in GitLab, with components being written in Rails and Go.
We work on a scale of processing a few million of CI/CD jobs on GitLab.com monthly.
CI/CD engineering is interlaced with a number of teams across GitLab.
We build new features by following our [direction](https://about.gitlab.com/direction/#ci--cd).
Currently, we focus on providing a deep integration of Kubernetes with GitLab:
1. by automating application testing and deployment through Auto DevOps,
1. by managing GitLab Runners on top of Kubernetes,
1. by working with other teams that provide facilities to monitor all running applications,
1. in the future implement A-B testing, feature flags, etc.

Additionally, we also focus on improving the efficiency, performance, and scalability of all aspects of CI/CD:
1. Improve performance of developer workflows, e.g. faster CI testing, by improving parallelization,
1. Improve performance of implementation, ex.:by allowing us to run 10-100x more in one year,
1. Identify and add features needed by us, ex.:to allow us to test more reliable and ship faster.

The CI/CD Engineering Manager also does weekly stand-up with a team and product managers to talk about plan for the work in the upcoming week and coordinates a deployment of CI/CD related services with infrastructure team.

1. Requirements
  * Go developer with a lot of Kubernetes production experience is a plus
  * Significant amount of experience with Ruby is a **strict requirement**

Candidates may be invited to schedule an interview with either an Engineer or Product Manager as part of the hiring process.

### Geo

[GitLab Geo](https://docs.gitlab.com/ee/gitlab-geo/README.html)
is an enterprise product feature that speeds up the work of globally distributed
teams, adds redundancy for GitLab instances, and provides Disaster Recovery as
well.

1. Geo Requirements (Staff Level)
  - Architect Geo and Disaster Recovery products for GitLab
  - Identify ways to test and improve availability and performance of GitLab Geo at GitLab.com scale
  - Instrument and monitor the health of distributed GitLab instances
  - Educate all team members on best practices relating to high availability
1. Requirements
  - Deep experience architecting and implementing fault-tolerant, distributed systems
  - Experience building and scaling highly-available systems
  - In-depth experience with Ruby on Rails, Go, and/or Git

### Edge

Edge Backend Developers are primarily tasked with improving the productivity of
the GitLab developers (from both GitLab Inc and the rest of the community), and
making the GitLab project maintainable in the long-term.

See the description of the [Edge team](/handbook/quality/edge) for more details. The position also involves working with the community as [merge request coach](/roles/merge-request-coach), and working together with our [issue triage specialists](/roles/specialist/issue-triage) to respond and address issues from the community.

### Monitoring

Monitoring developers work primarily on two aspects of GitLab: improving the instrumentation of GitLab itself, as well as building [features our customers can use to monitor their own applications](https://about.gitlab.com/direction/#monitoring).  This functionality includes metrics, tracing, and logging.


The requirements for a Monitoring developer are:
- An interest in enabling users to monitor deployed applications
- In-depth experience with Ruby on Rails is required. Other languages such as Go, C, Java is a plus
- Experience with Kubernetes, Docker, Linux system administration
- Experience developing, or utilizing, solutions to monitor production applications a plus

### BizOps

[BizOps](https://gitlab.com/gitlab-org/bizops) is an early stage project at GitLab focused on delivering an open source framework for analytics, business intelligence, and data science. It leverages version control, data science tools, CI, CD, Kubernetes, and review apps.

A BizOps developer will be tasked with executing on the vision of the BizOps project, to bring the product to market. The requirements for a BizOps developer include:
* A passion for data science and analytics
* Experience with doing initial prototyping, architecture, and engineering work
* In-depth experience with Python
* Experience with Kubernetes, Helm, and CI/CD is a **strict requirement**

### Database

A database specialist is a developer that focuses on database related changes
and improvements. You will spend the majority of your time making application
changes to improve database performance, availability, and reliability; though
you will also spend time working on the database infrastructure that powers
GitLab.com.

Unlike the [Database Engineer](/roles/engineering/database-engineer/) position the database
specialist title focuses more on application development and less on knowledge
of PostgreSQL. As such Ruby knowledge is absolutely required, but the
requirements for PostgreSQL knowledge / experience are less strict compared to
the Database Engineer position.

#### Example Projects

* Rewriting the database queries and related application logic used for retrieving [subgroups](https://docs.gitlab.com/ee/user/group/subgroups/index.html#subgroups)
* Rewriting code used for importing projects from other platforms (e.g. [GitHub](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14731))
* Adding trend analysis to monitoring to better detect performance and availability changes on GitLab.com
* Analyzing tables and optimizing them by adding indexes, breaking them up into separate tables, or by removing unnecessary columns.
* Reviewing database related changes submitted by other developers
* Documenting database best practices or patterns to avoid

#### Requirements

* At least 2 years of experience running PostgreSQL in production environments
* At least 5 years of experience working with Ruby
* At least 3 years of experience with Ruby on Rails or other Ruby frameworks
  such as Sinatra or Hanami
* Solid understanding of SQL
* Significant experience working in a distributed production environment

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/team/).

- Qualified applicants receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with our Recruiting team
- Next, candidates will be invited to schedule a behavioral interview with the hiring manager
- Next, candidates will be invited to schedule a [technical interview](https://about.gitlab.com/handbook/hiring/interviewing/technical/) with a team manager
- Candidates will then be invited to schedule an interview with Director of Backend
- Candidates will then be invited to schedule an additional interview with VP of Engineering
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).

<%= partial "includes/apply_and_about" %>
