---
layout: markdown_page
title: "Community Writers Program – Terms and Conditions"
twitter_image: '/images/tweets/community-writers-terms-and-conditions.png'
description: "Terms and Conditions applied to the GitLab Community Writers Program"
---

## Community Writers Program – Terms and Conditions
{:.no_toc}
For an overview, please check the [Community Writers Program](../) webpage.

----

## On this page
{:.no_toc}

- TOC
{:toc}

----

### Purpose of the program

The [Community Writers Program](../) has the purpose of sourcing high-quality stories for the [GitLab blog](http://about.gitlab.com/blog).

### Requirements

#### Essential

- Writers should have **excellent writing skills** and have **previous experience**
in writing technical content, which needs to be clear, correct, concise,
trustworthy, well structured, and easy to follow.
- Writers should have **excellent written English skills**.
- Writers should be fully capable of creating their own content and presenting
it **ready to publish**. We will not consider content that needs extensive reviews to get ready.
- Writers should **read all the guidelines** and **respect them entirely**. If you don't, we won't review your submission.
- Writers should **accept the review** and avoid confronting the reviewer endlessly;
do it only when strictly necessary. Reviewers are there to help to refine the content,
and to ensure it meets the standard of quality required by GitLab.
- Content must be **original and unprecedented**. Do not copy or even loosely paraphrase content from existing articles (from GitLab or anywhere else).

#### Desirable

- Writers should preferably be **experienced in writing in [Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)** and able to structure
their files without further assistance.
- Writers should preferably be active users of **Git and GitLab**, and must have a user
account on GitLab.com.

### Steps to getting published

The following stages are required to get your article published by GitLab.

#### 1. Topic

- Writers choose one of the existing issues labeled "[up-for-grabs](https://gitlab.com/gitlab-com/community-writers/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=up-for-grabs)", or create a new one in the
[Community Writers issue tracker](https://gitlab.com/gitlab-com/community-writers/issues).
**Pick only one issue at a time**.

#### 2. Pre-assessment

- Writers propose the outline of their articles in the issue thread,
together with a writing sample, and the introduction of their article.
  - **Writing sample**: Any relevant material previously written by the author,
  preferably on the same or a similar topic.
  - **Introduction**: Write the introduction of your article (100-150 words). We'll evaluate your approach, writing style, organization, clearness, and conciseness.
  - **Outline**: A brief description of  your post and what you intend to include in it. Think about why people would be interested in reading your story and why you're the best person to write it. Also consider what you want people to learn or take away from your post.
- Once you've gathered the outline, introduction and writing sample,
drop us a line in the issue thread to let us know you're willing to tackle it:

    ```md
    @rebecca I would like to write about this subject and I accept the
    [terms](https://about.gitlab.com/community-writers/terms-and-conditions/)
    of the Community Writers Program.
    ```

- The issue will be labeled "pre-assess".
- The editor evaluates the outline, writing sample and the introduction in the issue thread.
- Based on these, the editor will choose whether or not to approve the author to proceed.
- If approved, the author will have a green light to write the content and
send us the first draft. In this case, the editor will drop a line in the issue thread:

    ```md
    @user, you got it! Please write your draft in [Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/) and send
    it in a [new confidential issue](https://gitlab.com/gitlab-com/community-writers/issues/new)
    to this project. We do not open any attachments.
    ```

- If approved, the original issue will be labeled "draft" by the editor.
Both labels "pre-assess" and "up-for-grabs" will be removed.
- If not approved, the label "pre-assess" will be removed.

**Note:** if you just comment in the issue and don't provide us with the writing sample, introduction, and outline, we will not respond to your comment.
{:.note}

#### 3. Draft

- Writers should write based on our [**editorial style guide**](https://gitlab.com/gitlab-com/marketing/blob/master/content/editorial-style-guide.md).
- Writers should send us the first draft (written in **[Markdown](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)**) through
a new confidential issue to be created by the authors themselves. We will not
open any attachments.
The new issue will be **confidential**, preserving the author's
privacy during the evaluation of the article and the compensation offer,
and ensuring their content remains private (unpublished). This issue will be labeled
"confidential issue" and remain confidential.

#### 4. Evaluation

- We will respond to the new confidential issue thread as soon as we can,
accepting or rejecting the proposal according to the [evaluation criteria](#evaluation-criteria).
- The original issue will be labeled "evaluate".
- If we reject the content, we can either give the author one chance to improve it, or reject it entirely. In this case, the issue will be labeled "up-for-grabs", and the confidential issue will be closed.
- If we accept the content, we will proceed to review, and the original issue
will be labeled "review-in-progress". Both labels "draft" and "evaluate" will be removed.

#### 5. Review

- We will review the content and ask the author to make any necessary
changes, improvements, and adjustments.
- We will make the review as brief and direct as possible.
We expect the author to submit their draft ready to be published.
- Once the author has addressed or resolved comments from the review, we will provide them with
a compensation offer, according to the [compensation criteria](#compensation-criteria).

#### 6. Merge request

- If the author accepts the compensation offer, we create a merge request with the post.
- On the merge request, the editors will proceed with the final review:
they may ask the author to make further adjustments, or they can copy edit the article themselves.
- As soon as the review is complete, the issue will be labeled "approved",
and the label "review in progress" will be removed.

#### 7. Get published and get paid

- Once your content has been approved for publishing, it will be scheduled by the editor and the merge request will be merged. The article will most likely be
published on the [GitLab blog](http://about.gitlab.com/blog), but occasionally posts will be better suited to publishing in the [GitLab Documentation](https://docs.gitlab.com/) as a [Technical Article](https://docs.gitlab.com/ee/development/writing_documentation.html#technical-articles). The editor will inform you if this is the case.
- The original issue will be labeled "published".
- Once your content has been published, we will send you an invoice template to be filled with your personal data and instructions to proceed to receive the compensation previously agreed. GitLab will pay you in American Dollars (USD) from a bank account in the USA, via wired transfer to your bank account. Please check the [compensation limitations](#compensation-limitations) below.
- Both issues will be closed.

### Compensation criteria

Once your draft is approved to proceed to the [merge request stage](#6-merge-request), we will inform you
about the compensation range your article is eligible for. If you agree to
compensation offered, we will create the merge request.

The offer will be based on the **complexity** and/or **knowledge level** of your content:

| Complexity | Knowledge Level | Compensation Range |
| ---------- | --------------- | :----------------: |
| Simple | Beginner | **$50** to **$100** |
| Intermediary | Intermediate | **$100** to **$150** |
| Complex | Advanced | **$150** to **$200** |

The overall evaluation is based on these criteria, but the reviewer will
consider your article as a whole to define the compensation offer.

The amount is expressed in American Dollars (USD). Please check the [compensation limitations](#compensation-limitations) below.

### Evaluation criteria

The content will be evaluated based on:

- Readability
- Flow
- Structure
- Grammar
- Complexity
- Accuracy
- Respect for the editorial style guide

### Content ownership

- Once in a merge request, the [ownership](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/legal/individual_contributor_license_agreement.md) of that content is GitLab's, therefore,
the author **cannot** publish that content somewhere else until it's published by GitLab.
- Once the article gets published by GitLab, the author is welcome to reproduce the content
totally or partially, on their own blog or somewhere else, as long as they provide attribution to GitLab:

    ```
    This article was [originally published](link-to-article) by [GitLab](https://about.gitlab.com).
    ```

**Note:** Once the content is published, GitLab reserves the right to change,
rewrite, update, and remove the content totally, at GitLab's will.
{:.note}

#### Updates

If the content becomes out of date, we may ask the author to update it.
GitLab reserves the right to update it, rewrite it
(subjected to authorship updates), or remove it entirely.

### Time frame

Once you're approved to write the draft in the pre-assessment stage, we expect you
to have the content ready to be sent to us up to three working days after our response.

We expect to have your content published up to one month after the beginning of
the process. It can be concluded faster than that, or slower, depending on the actual demand of
work from our editors, and on the quality of your content. The editor will be able to give you a clearer idea of when to expect to see your post live once it is in a merge request.

### Ineligibility

You are considered **ineligible** for the Community Writers Program:

- If you're a GitLab Team member.
- If you intend to promote your own product, company, or the company you work for.
In this case, you might be eligible for a [Guest Blog Post](/handbook/marketing/blog/#guest-posts).
- If you don't fulfil the [requirements](#requirements) described above.

### Program manager

The Community Writers Program is part of the [Content Marketing](https://about.gitlab.com/handbook/marketing/marketing-sales-development/content/) Team at GitLab.
[Rebecca Dodd](/team/#rebecca), the **Content Editor**, manages the program.

### Important notes

- If you want to write about your own product (or the product you represent),
and how it integrates with GitLab, we'll be happy to have you as a
[Guest Writer](/handbook/marketing/blog/#guest-posts). The Community Writers Program
won't apply for these cases.
- You are encouraged to write more than one article. If you succeed in
the first process, we may invite you to write for us regularly.
The process for writing more than one article is exactly the same for
writing the first one. Please pick up only one issue at a time.
- GitLab reserves the right to refuse to publish any article at any time of the process.
- GitLab reserves the right to close the issue at any time, and not to proceed with that subject.
- Once you start the process by choosing a topic, you agree to the terms and
conditions established on the present document.
- The terms and conditions presented by this document are subject to
changes without previous notice.
- The terms and conditions presented by this document are subjected to [GitLab's DCO + License](#license).
- {:#compensation-limitations} We have some **limitations** for [doing business with a few countries](/handbook/sales-process/images_sales_process/#export-control-classification-and-countries-we-do-not-do-business-in-).
- If you are a resident of one of these countries,
please be aware that we may encounter difficulties in compensating you. In these cases,
and other cases in which you can't be compensated by USA-wired transfers in USD, we
urge you to bring this up **before** start writing. As an alternative compensation method,
we can give you a voucher for our [swag shop](https://shop.gitlab.com/).

## Developer Certificate of Origin + License
{:#license}

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following
[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md) terms.

Last update: 2017-11-29
{:.note .text-right}
